
dofile("HttpSimple.lua")

HttpServerESP8266 = {}

function HttpServerESP8266:new()
	local res = {
	}
	setmetatable(res, self)
	self.__index = self
	return res
end

function HttpServerESP8266:process(request,response)
	print ("HEADER--------\n".. request:getHeaderString() .."\n----\n");
	print ("CONTENT-------\n".. request:getContent() .."\n----\n")
	return response
end

function HttpServerESP8266:worker(request)
	-- create new request instance and initialize it with payload
	local requestObj=HttpSimple:new();
	requestObj:parseRequest(request);
	local responseObj=HttpSimple:new();
	-- call process
	return self:process(requestObj,responseObj);
end

function HttpServerESP8266:run(port)
	port = port or 80

	if net then
		srv=net.createServer(net.TCP) 
		srv:listen( port ,function(conn) 
			conn:on("receive",function(conn,request) 
				resonse=self:worker(request);
				conn:send(response:getResponse());
			end)
	
			conn:on("sent",function(conn)
				conn:close()
			end)
		end)
	end
end

--[[
vim: set ts=4:
]]
