-----------------------------------------------------------------------------
local string = require("string")
local base = _G
local table = require("table")


HttpSimple={}

function HttpSimple:new()
	local res = {
		post={},
		get={},
		requestUri=nil,
		host=nil,
		header={
			["Content-Type"] = "text/plain",
			["Content-Length"] = "0"
		},
		status=200,
		content='',
		method=nil,
		version=''
		
	}
	setmetatable(res, self)
	self.__index = self
	return res
end

function HttpSimple:setContent(content)
	self.content = content or ""
	self:setHeader("Content-Length",self:getContentLength());
end

function HttpSimple:setHeader(k,v)
	if not k then return end
	self.header[k]= v;
end

function HttpSimple:getHeader(k)
	return self.header[k];
end

function HttpSimple:setContentType(contentType)
	self.contentType = contentType
end

function HttpSimple:setStatus(status)
	self.status = status
end

function HttpSimple:getStatusText(status)
	local statusCodes={
		[200] = "OK",
		[301] = "Moved Permanently",
		[302] = "Found",
		[304] = "Not Modified",
		[400] = "Bad Request",
		[401] = "Unauthorized",
		[403] = "Forbidden",
		[404] = "Not Found",
		[405] = "Method Not Allowed",
		[409] = "Conflict",
		[500] = "Internal Server Error",
		[501] = "Not Implemented",
		[505] = "HTTP Version not supported"
	}
	return statusCodes[status] or "UNKNOWN"
end

function HttpSimple:getHeaderString()
	s="HTTP/1.0 ".. self:getStatus() .. " " .. self:getStatusText(self:getStatus()).. "\r\n"

	for k, v in pairs(self.header) do
		s=s .. k .. ":" .. v .. "\r\n"
	end
	return s
end


function HttpSimple:getContent()
	return self.content or ""
end

function HttpSimple:getContentLength()
	return self:getContent():len()
end

function HttpSimple:getStatus()
	return self.status
end

function HttpSimple:getContentType()
	return self.contentType or "text/html"
end

function HttpSimple:getResponse()
	return self:getHeaderString() .. self:getContent()
end

function HttpSimple:parseRequest(request)

	-- normalize eol
	request:gsub('\r\n','\n'):gsub('\r', '\n')

	local lastkey=''
	local content=''

	for line in request:gmatch("([^\n]*)\n") do
		if line == "" or content_start==1 then
			content_start=1
			if content=="" then content=content.."\n"..line else content=line end
		else
			-- parse request line
			if not self.method then
				self.method,self.requestUri,self.version = line:match "^(%w+)%s+([^%s]+)%s+([^%s]+)$" ;
				-- allow only POST and GET
				--if (self.method~="POST" and self.method~="GET") then error("status=405") end
			else
				local k,v=line:match "^([^:]+):%s(.*)$"
				if k and v then
					self:setHeader(k,v or '')
					lastkey=k
				else	
					-- append to last key
					lastvalue=(self:getHeader(lastkey) or '')
					self:setHeader(lastkey, lastvalue ..' '.. line:match "^%s*(.-)%s*$")
				end
			end
		end
	end

	self:parseRequestUri()

	self:setContent(content)
	if (self:getHeader("Content-Type") == "application/x-www-form-urlencoded") then
		for k,v in pairs( self:decodeUrl(content) ) do
			self:setPost(k,v)
		end
	end
end

function HttpSimple:parseRequestUri()
	local path,params=self.requestUri:match "^([^\?]+)\?(.*)$"

	for k,v in pairs( self:decodeUrl(params or '') ) do
		self:setGet(k,v)
	end
	
end

function HttpSimple:httpError(text)
	self:setStatus("400") 
	self:setContentType("text/plain") 
	self:setContent(text)
end

function HttpSimple:setGet(k,v)
	self.get[k]=v
end

function HttpSimple:getGet(k)
	return self.get[k]
end

function HttpSimple:setPost(k,v)
	self.post[k]=v
end

function HttpSimple:getPost(k)
	return self.post[k]
end

function HttpSimple:escape(v)
	v= v:gsub("([^A-Za-z0-9_])", function(c)
		return string.format("%%%02x", string.byte(c))
	end)
	v= v:gsub(" ", "+")
	return v;
end

function HttpSimple:unescape(v)
	v= v:gsub("+", " ")
	v= v:gsub("%%(%x%x)", function(hex)
		return string.char(base.tonumber(hex, 16))
	end)
	return v;
end

function HttpSimple:encodeUrl(params)
	local s= ""
	for k,v in pairs(params) do
		s= s .. "&" .. self:escape(k) .. "=" .. self:escape(v)
	end
	return s:sub(2)     -- remove first `&'
end


function HttpSimple:decodeUrl(s)
    param= {}
      for k, v in s:gfind( "([^&=]+)=([^&=]+)") do
       	k= self:unescape(k)
        v= self:unescape(v)
        param[k]= v
      end
    return param
end


